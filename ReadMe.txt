Huffman compression and decompression
--------------------------------------------------------------------------------------------------------------------------------
Command 			:
	compile 			=> g++ main.cpp huffman.cpp -o a.out
	run compression 	=> ./a.out input.txt output.txt c
	run decompression 	=> ./a.out output.txt input.txt d		