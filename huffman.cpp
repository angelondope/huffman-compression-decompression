#include"huffman.h"
#include<stack>

Huffman::Huffman()
{
    mOutputMemory = NULL;
}

Huffman::~Huffman()
{
	if (mOutputMemory)
		delete mOutputMemory;
	mOutputMemory = NULL; 
}

Huffman::Node::Node()
{
    this->mValue = '\0';
    this->mFrequency = 0;
    this->mLeft = NULL;
    this->mRight = NULL;
    this->mCharCode = 0;
    this->mCharCodeLength = 0;
}

Huffman::Node::Node(Byte value, int frequency)
{
    this->mValue = value;
    this->mFrequency = frequency;
    this->mLeft = NULL;
    this->mRight = NULL;
    this->mCharCode = 0;
    this->mCharCodeLength = 0;
}

Huffman::Node::Node(Byte value, int frequency, Node *left, Node *right)
{
    this->mValue = value;
    this->mFrequency = frequency;
    this->mLeft = left;
    this->mRight = right;
    this->mCharCode = 0;
    this->mCharCodeLength = 0;
}

Huffman::Node& Huffman::Node::operator=(const Node& rhs)
{
   this->mValue = rhs.mValue;
   this->mFrequency = rhs.mFrequency;
   this->mLeft = NULL;
   this->mRight = NULL;
   return *this;
}

int Huffman::Node::operator==(const Node& rhs) const
{
   if( this->mValue != rhs.mValue) return 0;
   if( this->mFrequency != rhs.mFrequency) return 0;
   return 1;
}

int Huffman::Node::operator<(const Node& rhs) const
{
   if(this->mFrequency < rhs.mFrequency) return 1;
   return 0;
}

//Compress file and return number of bytes
int Huffman::Compress(Byte* input, int inputlength)
{
    list<Node*> nodeList;
    int nodeListSize = 0;

    for(int i = 0; i < inputlength; i++)
    {
        int count = 0;
        for(int j = 0; j < inputlength; j++)
        {
            if(input[i] == input[j])
            {
                if(i > j)
                    break;
                else
                    count++;
            }
        }
        
        if(count > 0)
        {
            Node *node = new Node(input[i], count);
            nodeList.push_front(node);
            nodeListSize++;
        }
    }
    
    Node* root = MakeBinaryTree(nodeList, nodeListSize);
    SetBitsForCharacters(root,0,0);
    return SetOutputBytes(input, inputlength, root);
}

//Decompress the compressed file and return number of bytes
int Huffman::Decompress(Byte* bytes, int inputlength)
{
    Byte *input = bytes;
    
    int bytesWritten = 0;
    
    bytesWritten = *input++ << 24;
    bytesWritten ^= *input++ << 16;
    bytesWritten ^= *input++ << 8;
    bytesWritten ^= *input++;
    
    Byte *number = new Byte[bytesWritten];
    
    for(int i = 0; i < bytesWritten; i++)
        *(number + i) = *(input + i);
    
    input += bytesWritten;
    
    int length = 0;
    length = *input++ << 24;
    length ^= *input++ << 16;
    length ^= *input++ << 8;
    length ^= *input++;
    
    int traversalcount = 0;
    traversalcount = *input++ << 24;
    traversalcount ^= *input++ << 16;
    traversalcount ^= *input++ << 8;
    traversalcount ^= *input++;
    
    
    mOutputMemory = new Byte[length];
    
    Byte *postOrderBytes = new Byte[traversalcount];
    for(int i = 0; i < traversalcount; i++)
        *(postOrderBytes + i) = *(input + i);
    input += traversalcount;
    
    Node *root = MakeTreeFromPostorderTraversal(postOrderBytes, traversalcount);
    SetBitsForCharacters(root,0,0);
    SetOutputCharacters(number, root, length);
    return length;
}

//Translate compressed bytes to characters
void Huffman::SetOutputCharacters(Byte* input, Node *root, int inputLength)
{
    Byte *outptr = mOutputMemory;
    int bit = 0;
    Node *node;
    int b;
    int bytes = 1;
    for(int i = 0; i < inputLength; i++)
    {
	node = root;
	while(node->mCharCodeLength == 0)
        {
            b = ((*input) >> bit) &1;
            node = (b > 0) ? node->mRight :  node->mLeft;
            input+=( (bit >> 2) & (bit >> 1) & bit) & 1;
            bit++; 
            if(bit == 8)
                bytes++;
            bit&=7;
	}  
	(*outptr) = node->mValue;
	outptr ++;
    }
}

//Write compressed bytes
int Huffman::SetOutputBytes(Byte* input, int length, Node *root)
{
    mOutputMemory = new Byte[2 * length +  12];
    int traversalCount, charCodeLength, charCode, bitsWritten, bitsInAByteWritten = 0, bytesWritten = 1;
    
    Byte* output = mOutputMemory;
    Byte* ptr = output;
    *output++ = '\0';
    *output++ = '\0';
    *output++ = '\0';
    *output++ = '\0';
    
    for(int j = 0; j < length; j++)
    {
        Node *node = GiveNode(input[j], root);
        charCode = node->mCharCode;
        charCodeLength = node->mCharCodeLength;
        bitsWritten =0;
        for (int i = 0; i < charCodeLength; i++)
        {
            (*output) ^= ((charCode >> bitsWritten)&1) << bitsInAByteWritten;
            bitsWritten++;
            (*(output+1)) = '\0';
            output += ((bitsInAByteWritten >> 2) & (bitsInAByteWritten >> 1) & bitsInAByteWritten) &1;
            bitsInAByteWritten++; 
            if(bitsInAByteWritten == 8)
                bytesWritten++;
            bitsInAByteWritten&=7;
        }
    }
    
    *ptr++ = bytesWritten >> 24;
    *ptr++ = bytesWritten >> 16;
    *ptr++ = bytesWritten >> 8;
    *ptr++ = bytesWritten;
    
    output++;
    
    *output++ = length >> 24;
    *output++ = length >> 16;
    *output++ = length >> 8;
    *output++ = length;
    
    traversalCount = PostTraversalCount(root);
    *output++ = traversalCount >> 24;
    *output++ = traversalCount >> 16;
    *output++ = traversalCount >> 8;
    *output++ = traversalCount;
    
    WriteTreeInPostTraversal(root, &output);
    
    return bytesWritten + traversalCount + 12; 
}

//Write post-order traversal tree to compressed file
void Huffman::WriteTreeInPostTraversal(Node* root, Byte** output)
{
    if(root == NULL)
        return;
    WriteTreeInPostTraversal(root->mLeft, output);
    WriteTreeInPostTraversal(root->mRight, output);
    *((*output)++) = root->mValue;
}

//Post-order traversal nodes count
int Huffman::PostTraversalCount(Node* root)
{
    int i = 1;
    if(root == NULL)
        return 0;
    i += PostTraversalCount(root->mLeft);
    i += PostTraversalCount(root->mRight);
    return i;
}

//Find and return node
Huffman::Node* Huffman::GiveNode(Byte b, Node* root)
{
    if(root == NULL || root->mValue == b)
        return root;
    Node *temp = GiveNode(b, root->mLeft);
    if(temp != NULL && temp->mValue == b)
        return temp;
    return GiveNode(b, root->mRight);
}

//Create a tree from post-order traversal nodes
Huffman::Node* Huffman::MakeTreeFromPostorderTraversal(Byte* input, int inputLength)
{
    stack<Node*> nodeStack;
    for(int i = 0; i < inputLength; i++)
    {
        Byte b = *(input + i);
        Node *node = new Node(b,0);
        if(b != '\0')
        {
            nodeStack.push(node);
        }
        else
        {
            node->mRight = nodeStack.top();
            nodeStack.pop();
            node->mLeft = nodeStack.top();
            nodeStack.pop();
            nodeStack.push(node);
        }
    }
    Node *root = nodeStack.top();
    nodeStack.pop();
    return root;
}

//Create a binary tree with all the characters
Huffman::Node* Huffman::MakeBinaryTree(list<Node*> &nodeList, int size)
{
    while(size != 1)
    {
        int min = 0, secondMin = 1;
        int minF = 999999, secondMinF = 999999;
        Node *minNode = NULL, *secondMinNode = NULL;
        int i = 0;
        for(list<Node*>::iterator itr=nodeList.begin(); itr != nodeList.end(); ++itr)
        {
            int f = (**itr).mFrequency;
            
            if(f < minF)
            {
                secondMinF = minF;
                secondMin = min;
                secondMinNode = minNode;
                minF = f;
                min = i;
                minNode = *itr;
            }
            else if(f <= secondMinF)
            {
                secondMinF = f;
                secondMin = i;
                secondMinNode = *itr;
            }
            i++;
        }

        nodeList.remove(minNode);
        nodeList.remove(secondMinNode);
        nodeList.insert(nodeList.end(), new Node('\0', minF+secondMinF, minNode, secondMinNode));
        size--;
    }
    
    return nodeList.front();
}

//Assign bits to characters
void Huffman::SetBitsForCharacters(Node* root, int charCode, int charCodeLength)
{
    bool leaf = true;
    if (root->mRight)
    {      
        SetBitsForCharacters(root->mRight, charCode ^ (1 << charCodeLength), charCodeLength+1);
	leaf = false;                                             
    }       
    if (root->mLeft)
    {
	SetBitsForCharacters(root->mLeft, charCode, charCodeLength+1);
	leaf = false;                               
    }  
    if (leaf)
    {
	root->mCharCodeLength = charCodeLength;
	root->mCharCode = charCode;
    }         
}

//Get the output memory
Byte* Huffman::GetOutput()
{
	if (mOutputMemory)
		return mOutputMemory;
	return NULL;
}