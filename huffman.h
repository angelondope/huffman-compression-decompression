#include<list>
#include<iostream>
using namespace std;

#ifndef HUFFMAN_H
#define HUFFMAN_H

typedef unsigned char Byte;
class Huffman
{
    private:
        class Node
        {
            public:
                int mFrequency, mCharCode, mCharCodeLength;
                Byte mValue;
                Node *mLeft, *mRight;
                Node();
                Node(Byte value, int frequency);
                Node(Byte value, int frequency, Node *left, Node *right);
                Node &operator=(const Node& rhs);
                int operator==(const Node& rhs) const;
                int operator<(const Node& rhs) const;
                ~Node()
                {
                    if (mLeft) {delete mLeft;}
                    if (mRight) {delete mRight;}
		}
        };

        int PostTraversalCount(Node*);
        void WriteTreeInPostTraversal(Node*, Byte**);
        Node* GiveNode(Byte, Node*);
        int SetOutputBytes(Byte*, int, Node*);
        Node* MakeBinaryTree(list<Node*>&, int);
        Node* MakeTreeFromPostorderTraversal(Byte*, int);
        void SetOutputCharacters(Byte*, Node*, int);
        void SetBitsForCharacters(Node*, int, int);
        Byte *mOutputMemory;
    public:
        Huffman();
        ~Huffman();
        int Compress(Byte *input, int inputlength);
	int Decompress(Byte *input, int inputlength);
	Byte* GetOutput();
};


#endif	// HUFFMAN_H

