#include"huffman.h"
#include <fstream>
#include <cstring>

int main(int argc, char** argv)
{    
    if(argc < 4)
    {
        cout<<"Input is invalid";
        return 0;
    }
    
    Byte op = (Byte)argv[3][0];
    
    if(op != 'c' && op != 'd')
    {
        cout<<"Input is invalid : "<<argv[3];
        return 0;
    }
    
    ifstream inFile(argv[1], ifstream::binary);
    ofstream outFile(argv[2], ofstream::binary);

    if(!inFile)
    {
        cout<<"Input file does not exist";
        return 0;
    }
    if(!outFile)
    {
        cout<<"Output file does not exist";
        inFile.close();
        return 0;
    }
    int length = -1;
    
    inFile.seekg(0, inFile.end);
    length = inFile.tellg();
    inFile.seekg(0,inFile.beg);
    
    if(length <= 0)
        return 0;
    
    Byte *output = NULL, *input = new Byte[length];
    char *characterInput = new char[length];
    int outSize = -1;
    Huffman *huff = new Huffman();
    
    inFile.read(characterInput, length);
    memcpy(input, characterInput, length);
    
    if(op == 'c')
        outSize = huff->Compress(input,length);
    else if(op == 'd')
        outSize = huff->Decompress(input,length);
    
    if(outSize > 0)
    {
        output = huff->GetOutput();
        char *outputCharacters = new char[outSize];
        memcpy(outputCharacters,output, outSize);
        outFile.write(outputCharacters,outSize);
    }
    
    inFile.close();
    outFile.close();
    
    delete huff;
    delete input;
    
    return 0;
}

